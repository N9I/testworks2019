﻿using System.IO;
using System.Xml.Serialization;
using XmlReader.Model.Dto;

namespace XmlReader.Helpers
{
    public static class XmlHelper
    {
        public static T ParseXml<T>(string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                var serializer = new XmlSerializer(typeof(DocumentDto));
                return (T)serializer.Deserialize(stream);
            }
        }
    }
}
