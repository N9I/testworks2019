﻿using System.Data;

namespace XmlReader.Helpers
{
    internal static class MathHelper
    {
        public static string Evaluate(string expression)
        {
            var table = new DataTable();
            table.Columns.Add("expression", typeof(string), expression.Replace(',', '.'));
            var row = table.NewRow();
            table.Rows.Add(row);
            return double.TryParse((string)row["expression"], out double result) ? result.ToString("0.###") : "";
        }
    }
}