﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using XmlReader.Model;

namespace XmlReader.Services
{
    public class ReportService : IReportService
    {
        public void SaveReport(string fileName, ObservableCollection<Resource> resources)
        {
            var msExcel = new Microsoft.Office.Interop.Excel.Application();
            var workBook = msExcel.Workbooks.Add(1);
            var workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.Sheets[1];
            workSheet.Cells[2][1] = "Код";
            workSheet.Cells[3][1] = "Название";
            workSheet.Cells[4][1] = "Единицы измерения";
            workSheet.Cells[5][1] = "Количество";
            workSheet.Rows[1].Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            workSheet.Rows[1].Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            workSheet.Rows[1].RowHeight = 29;
            workSheet.Columns[2].NumberFormat = "@";
            workSheet.Columns["A:E"].NumberFormat = "@";
            workSheet.Columns["A:E"].Interior.TintAndShade = 0;
            workSheet.Columns["A:E"].Interior.PatternTintAndShade = 0;
            workSheet.Columns["A:E"].WrapText = true;
            workSheet.Columns["A"].ColumnWidth = 4;
            workSheet.Columns["B"].ColumnWidth = 18;
            workSheet.Columns["C"].ColumnWidth = 50;
            workSheet.Columns["D"].ColumnWidth = 18;
            workSheet.Columns["E"].ColumnWidth = 18;
            var index = 2;
            foreach (var chapter in resources.Select(r => r.Position.Chapter).Distinct().OrderBy(c => c.Caption))
            {
                workSheet.Cells[index, 1] = chapter.Caption;
                workSheet.Range[workSheet.Cells[index, 1], workSheet.Cells[index, 5]].Merge();
                workSheet.Rows[index].Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                workSheet.Rows[index].Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                workSheet.Rows[index].RowHeight = 29;
                index++;
                var chapterIndex = index;
                foreach (var position in resources.Where(r => r.Position.Chapter == chapter).Select(r => r.Position).Distinct().OrderBy(c => c.Order))
                {
                    workSheet.Cells[1][index] = position.Order;
                    workSheet.Cells[2][index] = position.Code;
                    workSheet.Cells[3][index] = position.Caption;
                    workSheet.Cells[4][index] = position.Units;
                    workSheet.Cells[5][index] = position.Quantity;
                    index++;
                    var positionIndex = index;
                    foreach (var resource in resources.Where(r => r.Position == position))
                    {
                        if (string.IsNullOrEmpty(resource.Code) &&
                            string.IsNullOrEmpty(resource.Caption) &&
                            string.IsNullOrEmpty(resource.Units) &&
                            string.IsNullOrEmpty(resource.Quantity))
                            continue;
                        workSheet.Cells[2][index] = resource.Code;
                        workSheet.Cells[3][index] = resource.Caption;
                        workSheet.Cells[4][index] = resource.Units;
                        workSheet.Cells[5][index] = resource.Quantity;
                        index++;
                    }
                    if (resources.Any(r => r.Position == position && !string.IsNullOrEmpty(r.Caption)))
                        workSheet.Rows[$"{positionIndex}:{index - 1}"].Group();
                }
                if (resources.Any(r => r.Position.Chapter == chapter && !string.IsNullOrEmpty(r.Caption)))
                    workSheet.Rows[$"{chapterIndex}:{index - 1}"].Group();
            }

            if (msExcel.ActiveWorkbook != null)
                msExcel.ActiveWorkbook.SaveAs(fileName);

            if (msExcel != null)
            {
                if (workBook != null)
                {
                    if (workSheet != null)
                        Marshal.ReleaseComObject(workSheet);
                    workBook.Close(false, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(workBook);
                }
                msExcel.Quit();
                Marshal.ReleaseComObject(msExcel);
            }
        }
    }
}
