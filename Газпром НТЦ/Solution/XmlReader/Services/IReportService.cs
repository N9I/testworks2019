﻿using System.Collections.ObjectModel;
using XmlReader.Model;

namespace XmlReader.Services
{
    public interface IReportService
    {
        void SaveReport(string filename, ObservableCollection<Resource> resources);
    }
}
