﻿using System.Collections.Generic;
using XmlReader.Model;
using XmlReader.Model.Dto;

namespace XmlReader.Services
{
    public interface IResourceService
    {
        IList<Resource> GetResorcesList(DocumentDto document);
    }
}