﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using XmlReader.Model;
using XmlReader.Model.Dto;
using static XmlReader.Helpers.MathHelper;

namespace XmlReader.Services
{
    public class ResourceService : IResourceService
    {
        public IList<Resource> GetResorcesList(DocumentDto document)
        {
            var resources = new List<Resource>();
            var dataTable = new DataTable();
            foreach (var chapterDto in document.Chapters)
            {
                var chapter = new Chapter { Caption = chapterDto.Caption };
                var positionOrder = 0;
                foreach (var positionDto in chapterDto.Positions)
                {
                    positionOrder++;
                    var position = new Position
                    {
                        Caption = positionDto.Caption,
                        Code = positionDto.Code,
                        Order = positionOrder,
                        Quantity = $"{Evaluate(positionDto.Quantity.Formula)}\r\n{positionDto.Quantity.Formula}",
                        Units = positionDto.Units,
                        Chapter = chapter
                    };
                    if (positionDto.Resources != null)
                    {
                        var resourcesDto = new List<ResourceItemDto>();
                        resourcesDto.AddRange(positionDto.Resources.Mat);
                        resourcesDto.AddRange(positionDto.Resources.Mch);
                        resourcesDto.AddRange(positionDto.Resources.Tzm);
                        resourcesDto.AddRange(positionDto.Resources.Tzr);
                        resources.AddRange(resourcesDto.Select(r => new Resource
                        {
                            Caption = r.Caption,
                            Code = r.Code,
                            Quantity = r.Quantity,
                            Units = r.Units,
                            Position = position
                        }));
                    }
                    else
                        resources.Add(new Resource { Position = position });
                }
            }
            return resources;
        }
    }
}
