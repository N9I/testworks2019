﻿namespace XmlReader.Model
{
    public class Resource
    {
        public string ChapterCaption { get; set; }
        public string Code { get; set; }
        public string Caption { get; set; }
        public string Units { get; set; }
        public string Quantity { get; set; }

        public Position Position { get; set; }
    }
}
