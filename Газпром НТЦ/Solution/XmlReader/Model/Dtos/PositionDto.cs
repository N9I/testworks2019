﻿using System.Xml;
using System.Xml.Serialization;

namespace XmlReader.Model.Dto
{
    public class PositionDto
    {
        [XmlAttribute("Caption")]
        public string Caption { get; set; }
        [XmlAttribute("Code")]
        public string Code { get; set; }
        [XmlAttribute("Units")]
        public string Units { get; set; }
        [XmlElement("Resources")]
        public ResourceListDto Resources { get; set; }
        [XmlElement("Quantity")]
        public PositionQuantityDto Quantity { get; set; }
    }

}
