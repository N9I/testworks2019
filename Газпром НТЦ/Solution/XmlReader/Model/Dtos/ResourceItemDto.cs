﻿using System.Xml;
using System.Xml.Serialization;

namespace XmlReader.Model.Dto
{
    public class ResourceItemDto
    {
        [XmlAttribute("Caption")]
        public string Caption { get; set; }
        [XmlAttribute("Code")]
        public string Code { get; set; }
        [XmlAttribute("Units")]
        public string Units { get; set; }
        [XmlAttribute("Quantity")]
        public string Quantity { get; set; }
    }

}
