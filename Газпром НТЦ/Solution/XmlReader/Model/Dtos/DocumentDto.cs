﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlReader.Model.Dto
{
    [XmlRoot("Document")]
    public class DocumentDto
    {
        [XmlArray("Chapters")]
        [XmlArrayItem("Chapter")]
        public List<ChapterDto> Chapters { get; set; }
    }

}
