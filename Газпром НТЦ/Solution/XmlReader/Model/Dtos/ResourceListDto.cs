﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace XmlReader.Model.Dto
{
    public class ResourceListDto
    {
        [XmlElement("Tzr")]
        public List<ResourceItemDto> Tzr { get; set; }
        [XmlElement("Tzm")]
        public List<ResourceItemDto> Tzm { get; set; }
        [XmlElement("Mch")]
        public List<ResourceItemDto> Mch { get; set; }
        [XmlElement("Mat")]
        public List<ResourceItemDto> Mat { get; set; }
    }

}
