﻿using System.Xml;
using System.Xml.Serialization;

namespace XmlReader.Model.Dto
{
    public class PositionQuantityDto
    {
        [XmlAttribute("Fx")]
        public string Formula { get; set; }
        [XmlAttribute("Result")]
        public string Result { get; set; }
    }

}
