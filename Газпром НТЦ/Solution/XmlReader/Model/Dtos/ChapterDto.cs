﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace XmlReader.Model.Dto
{
    public class ChapterDto
    {
        [XmlAttribute("Caption")]
        public string Caption { get; set; }
        [XmlElement("Position")]
        public List<PositionDto> Positions { get; set; }
    }

}
