﻿namespace XmlReader.Model
{
    public class Position
    {
        public int Order { get; set; }
        public string Code { get; set; }
        public string Caption { get; set; }
        public string Units { get; set; }
        public string Quantity { get; set; }

        public Chapter Chapter { get; set; }
    }
}