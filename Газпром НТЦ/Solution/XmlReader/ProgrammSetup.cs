﻿using Microsoft.Extensions.DependencyInjection;
using XmlReader.Services;
using XmlReader.ViewModel;

namespace XmlReader
{
    public partial class MainWindow
    {
        private ServiceProvider SetupServices()
        {
            return new ServiceCollection()
                .AddScoped<IMainWindowViewModel, MainWindowViewModel>()
                .AddScoped<IReportService, ReportService>()
                .AddScoped<IResourceService, ResourceService>()
                .BuildServiceProvider();
        }
    }
}