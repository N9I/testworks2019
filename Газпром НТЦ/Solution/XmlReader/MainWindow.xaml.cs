﻿using Microsoft.Extensions.DependencyInjection;
using System.Windows;
using XmlReader.ViewModel;

namespace XmlReader
{

    public partial class MainWindow : Window
    {
        private ServiceProvider _container;

        public MainWindow()
        {
            InitializeComponent();
            _container = SetupServices();
            DataContext = _container.GetService<IMainWindowViewModel>();
        }
    }
}
