﻿using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using XmlReader.Model;
using XmlReader.Model.Dto;
using XmlReader.Services;
using static XmlReader.Helpers.XmlHelper;

namespace XmlReader.ViewModel
{

    public class MainWindowViewModel : INotifyPropertyChanged, IMainWindowViewModel
    {
        private IResourceService _resourceService;
        private IReportService _reportService;
        private string _status = string.Empty;
        private RelayCommand _openCommand;
        private RelayCommand _exportCommand;

        public ObservableCollection<Resource> Resources { get; set; } = new ObservableCollection<Resource>();

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        public RelayCommand OpenCommand
        {
            get
            {
                return _openCommand ??
                  (_openCommand = new RelayCommand(obj =>
                  {
                      try
                      {
                          var openFileDialog = new OpenFileDialog
                          {
                              DefaultExt = "xml",
                              Filter = "xml (*.xml)|*.xml|All files (*.*)|*.*",
                              InitialDirectory = AppDomain.CurrentDomain.BaseDirectory
                          }; ;
                          if (openFileDialog.ShowDialog() == true)
                          {
                              Status = "Идет чтение файла ...";
                              LoadContent(openFileDialog.FileName);
                              Status = openFileDialog.FileName.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
                          }
                      }
                      catch (Exception)
                      {
                          Resources.Clear();
                          Status = "Ошибка загрузки файла. Попробуйте снова или выберите другой файл";
                      }
                  }));
            }
        }

        public RelayCommand ExportCommand
        {
            get
            {
                return _exportCommand ??
                  (_exportCommand = new RelayCommand(obj =>
                  {
                      var saveFileDialog = new SaveFileDialog {
                          DefaultExt = "xls",
                          Filter = "xls (*.xls)|*.xls|All files (*.*)|*.*",
                          InitialDirectory = AppDomain.CurrentDomain.BaseDirectory
                      };

                      if (saveFileDialog.ShowDialog() == true)
                        _reportService.SaveReport(saveFileDialog.FileName, Resources);
                      
                  }));
            }
        }

        public MainWindowViewModel(IResourceService resourceService, IReportService reportService)
        {
            _resourceService = resourceService;
            _reportService = reportService;
            StartMainWindow();
        }

        private void StartMainWindow()
        {
            var fileName = "ЛС 02-01-01-01.xml";
            if (File.Exists(fileName))
            {
                _status = "Идет чтение файла ...";
                LoadContent(fileName);
                _status = fileName.Replace(AppDomain.CurrentDomain.BaseDirectory, "");
            }
            else
                _status = "Начните работу с выбора файла";

        }

        private void LoadContent(string fileName)
        {
            try
            {
                Resources.Clear();
                var documentDto = ParseXml<DocumentDto>(fileName);
                var resources = _resourceService.GetResorcesList(documentDto);
                foreach (var resource in resources)
                    Resources.Add(resource);
            }
            catch (Exception)
            {
                Status = "Ошибка загрузки файла. Попробуйте снова или выберите другой файл";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
