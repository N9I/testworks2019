﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using XmlReader.Model;

namespace XmlReader.ViewModel
{
    public interface IMainWindowViewModel: INotifyPropertyChanged
    {
        RelayCommand OpenCommand { get; }
        RelayCommand ExportCommand { get; }
        ObservableCollection<Resource> Resources { get; set; }
        string Status { get; set; }
    }
}