var timenow=0; 
var buttonNames = ['button1','button2','button3','button4']

function ButtonClick(clicked_id) {
	var buttons = Array.from(document.getElementById("TestButton").getElementsByTagName('button'));
	buttons.forEach(function(button, i, buttons){
		button.removeAttribute("disabled");
	});
	document.getElementById(clicked_id).setAttribute("disabled", "disabled");
}

angular.module('TestZapSibKomBankApp', [])
	.controller('TimerController', function($scope, $interval) {
		var updateClock = function() {
			timenow++;
			$scope.timer = (timenow > 59)
				?('0'+((timenow - timenow % 60) / 60)).slice(-2)+':'+('0'+(timenow%60)).slice(-2)
				:('0'+(timenow%60)).slice(-2);
		};
		$scope.TimerStar = function() {
			$interval(updateClock, 1000);
			$scope.buttonVisible = "false";
		};
	})
	.controller('TimeLimitController', function($scope) {
		$scope.DateSet = function () {
			var mdy = $scope.limit.split('.');
			var diff = Math.round(new Date(mdy[2], mdy[1]-1, mdy[0])- new Date());
			var absDiff = Math.abs(diff);
			var days = (absDiff - absDiff % (1000*60*60*24)) / (1000*60*60*24);
			absDiff = absDiff % (1000*60*60*24);
			var hours = (absDiff - absDiff % (1000*60*60)) / (1000*60*60);
			absDiff = absDiff % (1000*60*60);
			var minutes = (absDiff - absDiff % (1000*60)) / (1000*60);
			$scope.timeLimit = " " + (diff>0 ? "До указанной даты осталось " : "Указанная дата прошла ") 
			+ (days > 0 ? "дней: " + days + " ": "")
			+ (hours > 0 ? "часов: " + hours + " ": "")
			+ (minutes >0 ? "минут: " + minutes + " ": "")
			+ (diff>0 ? "" : "назад")
          };
		$('#datepickertest').datepicker({
			position: "right top", 
			autoClose: true,
			onSelect: function(formattedDate, date, inst){			
				var event = document.createEvent("HTMLEvents");
				event.initEvent("change", false, true);
				document.getElementById("datepickertest").dispatchEvent(event);
			}	
		});
	})
	.controller('ButtonController', function($scope) {
		var first = true;
		buttonNames.forEach(function(item, i, buttonNames){
			var myDiv = document.getElementById("TestButton");
			var button = document.createElement('BUTTON');
			var text = document.createTextNode(item);
			button.setAttribute("id",item);
			button.setAttribute("onclick","ButtonClick(this.id)");
			if (first) {
				button.setAttribute("disabled", "disabled");
				first = false;
			}
			button.appendChild(text);
			myDiv.appendChild(button); ;
		});
	})
	.controller('ButtonController2', function($scope) {
		$scope.buttons = buttonNames;
		$scope.ButtonClick = function($event, button) {
			var buttons = Array.from(angular.element($event.currentTarget).parent().find( "button" ));
			buttons.forEach(function(button, i, buttons){
				button.removeAttribute("disabled");
			});
			$event.currentTarget.setAttribute("disabled", "disabled");
		};
	})