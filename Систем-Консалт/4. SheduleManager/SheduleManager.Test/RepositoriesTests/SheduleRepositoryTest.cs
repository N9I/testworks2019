using Microsoft.VisualStudio.TestTools.UnitTesting;
using SheduleManager.Model;
using SheduleManager.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using static SheduleManager.Test.MeetingGenerateHelper;

namespace SheduleManager.Test
{
    [TestClass]
    public class SheduleRepositoryTest
    {
        [TestMethod]
        public void Create_Meeting_ItemAdd()
        {
            var meeting = GetMeetingNow();
            var id = meeting.Id;
            var repository = new SheduleRepository();

            repository.Create(meeting);

            Assert.AreEqual(meeting, repository.Get(id));
            Assert.AreEqual(1, repository.GetAll().Count);
        }

        [TestMethod]
        public void Create_Meeting_ItemReturned()
        {
            var meeting = GetMeetingAddTime(1);
            var id = meeting.Id;
            var repository = new SheduleRepository();

            var result = repository.Create(meeting);

            Assert.AreEqual(meeting, result);
        }

        [TestMethod]
        public void Get_ExistedItemById_ItemReturned()
        {
            var meeting = GetMeetingAddTime(1);
            var id = meeting.Id;
            var repository = new SheduleRepository(new List<Meeting> { meeting });

            var result = repository.Get(id);

            Assert.AreEqual(meeting, result);
        }

        [TestMethod]
        public void Get_NotExistedItemById_NullReturned()
        {
            var meeting = GetMeetingAddTime(1);
            var id = Guid.NewGuid().GetHashCode().ToString("x");
            var repository = new SheduleRepository(new List<Meeting> { meeting });

            var result = repository.Get(id);

            Assert.AreNotEqual(meeting, result);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void Get_ItemsOnlyByDate_2ItemsReturned()
        {
            var testName = "test3";
            var listMeeting = new List<Meeting> { GetMeetingAddTime(1), GetMeetingAddTime(1, 1), GetMeetingAddTime(2, 0,testName) };
            var repository = new SheduleRepository(listMeeting);

            var result = repository.Get(DateTime.Now.AddDays(1));

            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            listMeeting.Remove(listMeeting.First(i => i.Name == testName));
            CollectionAssert.AreEqual(listMeeting, result.ToList());
        }

        [TestMethod]
        public void Get_ItemsByDateWithDateEndEqual_2ItemsReturned()
        {
            var testName = "test3";
            var listMeeting = new List<Meeting> { GetMeetingWitDiffereDayBeginAndEnd(), GetMeetingAddTime(1, 1), GetMeetingAddTime(2, 0, testName) };
            var repository = new SheduleRepository(listMeeting);

            var result = repository.Get(DateTime.Now.AddDays(1));

            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            listMeeting.Remove(listMeeting.First(i => i.Name == testName));
            CollectionAssert.AreEqual(listMeeting, result.ToList());
        }

        [TestMethod]
        public void Get_ItemsByDateFromNotOrderList_2OrderedItemsReturned()
        {
            var testName = "test3";
            var listMeeting = new List<Meeting> { GetMeetingAddTime(2, 0, testName), GetMeetingAddTime(1, 1), GetMeetingWitDiffereDayBeginAndEnd()};
            var repository = new SheduleRepository(listMeeting);

            var result = repository.Get(DateTime.Now.AddDays(1));

            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            listMeeting.Remove(listMeeting.First(i => i.Name == testName));
            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            CollectionAssert.AreEqual(listMeeting.OrderBy(l => l.DateBegin).ToList(), result.ToList());
        }

        [TestMethod]
        public void GetAll_2Item_2ItemsReturned()
        {
            var listMeeting = new List<Meeting> { GetMeetingAddTime(1), GetMeetingAddTime(2) };
            var repository = new SheduleRepository(listMeeting);

            var result = repository.GetAll();

            CollectionAssert.AreEqual(listMeeting, result.ToList());
        }

        [TestMethod]
        public void Update_Meeting_UpdatedItemsReturned()
        {
            var listMeeting = new List<Meeting> { GetMeetingAddTime(1), GetMeetingAddTime(2) };
            var meeting3 = GetMeetingAddTime(3);
            meeting3.Id = listMeeting.First().Id;
            var repository = new SheduleRepository(listMeeting);

            var result = repository.Update(meeting3);

            Assert.AreEqual(meeting3, result);
        }

        [TestMethod]
        public void Update_Meeting_UpdateItem()
        {
            var meeting1 = GetMeetingAddTime(1);
            var meeting2 = GetMeetingAddTime(2);
            var meeting3 = GetMeetingAddTime(3);
            meeting3.Id = meeting1.Id;
            var repository = new SheduleRepository(new List<Meeting> { meeting1, meeting2 });

            repository.Update(meeting3);

            Assert.AreEqual(meeting3, repository.Get(meeting3.Id));
            var meetingList = repository.GetAll();
            CollectionAssert.AreNotEqual(new List<Meeting> { meeting1, meeting2 }, meetingList.ToList());
            CollectionAssert.AllItemsAreUnique(meetingList.ToList());
            Assert.AreEqual(2, meetingList.Count);
            Assert.AreNotEqual(meeting3, meetingList.First(m => m.Id != meeting3.Id));
        }

        [TestMethod]
        public void Delete_Meeting_DeleteItem()
        {
            var meeting1 = GetMeetingAddTime(1);
            var meeting2 = GetMeetingAddTime(2);
            var meeting3 = GetMeetingAddTime(3);
            var id = meeting2.Id;
            var repository = new SheduleRepository(new List<Meeting> { meeting1, meeting2, meeting3 });

            repository.Delete(meeting2);

            Assert.IsNull(repository.Get(id));
            Assert.IsNull(repository.Get(meeting2.Id));
            var meetingList = repository.GetAll();
            CollectionAssert.AreEqual(new List<Meeting> { meeting1, meeting3 }, meetingList.ToList());
            CollectionAssert.AllItemsAreUnique(meetingList.ToList());
            Assert.AreEqual(2, meetingList.Count);
            Assert.AreNotEqual(new List<Meeting> { meeting1, meeting2, meeting3 }, meetingList.ToList());
        }

        [TestMethod]
        public void RangeBusy_DateBeginInDiapason_TrueReturned()
        {
            var meeting = GetMeetingNow();
            meeting.DateBegin = meeting.DateBegin.AddMinutes(15);
            meeting.DateEnd = meeting.DateEnd.AddHours(1).AddMinutes(15);
            var repository = new SheduleRepository(new List<Meeting> { GetMeetingNow(), GetMeetingAddTime(1) });

            var result = repository.RangeBusy(meeting);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RangeBusy_DateEndInDiapason_TrueReturned()
        {
            var meeting = GetMeetingNow();
            meeting.DateBegin = meeting.DateBegin.AddHours(2);
            meeting.DateEnd = meeting.DateEnd.AddHours(23).AddMinutes(15);
            var repository = new SheduleRepository(new List<Meeting> { GetMeetingNow(), GetMeetingAddTime(1) });

            var result = repository.RangeBusy(meeting);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RangeBusy_RangeNotInDiapason_FalseReturned()
        {
            var meeting = GetMeetingNow();
            meeting.DateBegin = meeting.DateBegin.AddHours(2);
            meeting.DateEnd = meeting.DateEnd.AddHours(2);
            var repository = new SheduleRepository(new List<Meeting> { GetMeetingNow(), GetMeetingAddTime(1) });

            var result = repository.RangeBusy(meeting);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RangeBusy_SearchInClearShedule_FalseReturned()
        {
            var repository = new SheduleRepository();
            var meeting = GetMeetingNow();
            meeting.DateBegin = meeting.DateBegin.AddHours(2);
            meeting.DateEnd = meeting.DateEnd.AddHours(2);

            var result = repository.RangeBusy(meeting);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void RangeBusy_SearchDiapasonIntoExistMeeting_TrueReturned()
        {
            var repository = new SheduleRepository(new List<Meeting> { GetMeetingNow() });
            var meeting = GetMeetingNow();
            meeting.DateBegin = meeting.DateBegin.AddMinutes(10);
            meeting.DateEnd = meeting.DateEnd.AddMinutes(-10);

            var result = repository.RangeBusy(meeting);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void RangeBusy_ExistMeetingIntoSearchDiapason_TrueReturned()
        {
            var meeting = GetMeetingNow();
            meeting.DateBegin = meeting.DateBegin.AddMinutes(10);
            meeting.DateEnd = meeting.DateEnd.AddHours(2).AddMinutes(20);
            var repository = new SheduleRepository(new List<Meeting> { GetMeetingAddTime(0,1) });

            var result = repository.RangeBusy(meeting);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetByNotificatoin_1ItemWithDateNotification1ItemWithWrongDateNotification_1ItemReturned()
        {
            var notification = DateTime.Now;
            var meeting = GetMeetingNow();
            meeting.Notification = notification;
            var meeting2 = GetMeetingNow();
            meeting2.Notification = notification.AddHours(1);
            var listMeeting = new List<Meeting> { meeting, meeting2 };
            var repository = new SheduleRepository(listMeeting);
            
            var result = repository.GetByNotificatoin(notification);

            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            listMeeting.Remove(meeting2);
            CollectionAssert.AreEqual(listMeeting.OrderBy(l => l.DateBegin).ToList(), result.ToList());
        }

        [TestMethod]
        public void GetByNotificatoin_2ItemsWithDateNotification1ItemWithWrongDateNotificationFromNotOrderList_2OrderedItemsReturned()
        {
            var notification = DateTime.Now;
            var meeting = GetMeetingAddTime(2);
            meeting.Notification = notification;
            var meeting2 = GetMeetingAddTime(1);
            meeting2.Notification = notification;
            var meeting3 = GetMeetingAddTime(3);
            var listMeeting = new List<Meeting> { meeting, meeting2, meeting3 };
            var repository = new SheduleRepository(listMeeting);

            var result = repository.GetByNotificatoin(notification);

            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            listMeeting.Remove(meeting3);
            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            CollectionAssert.AreEqual(listMeeting.OrderBy(l => l.DateBegin).ToList(), result.ToList());
        }
    }
}
