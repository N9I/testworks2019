using Microsoft.VisualStudio.TestTools.UnitTesting;
using SheduleManager.Model;
using SheduleManager.Repositories;
using SheduleManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using static SheduleManager.Test.MeetingGenerateHelper;

namespace SheduleManager.Test
{
    [TestClass]
    public class SheduleServiceTest
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MeetingAdd_DateBeginLowDateNow_ExpectedException()
        {
            var meeting = GetMeetingAddTime(-1);
            var repository = new SheduleRepository();
            var service = new SheduleService(repository);

            service.MeetingAdd(meeting);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MeetingAdd_DateEndLowDateBegin_ExpectedException()
        {
            var meeting = GetMeetingAddTime(3);
            meeting.DateEnd = meeting.DateEnd.AddHours(-2);
            var repository = new SheduleRepository();
            var service = new SheduleService(repository);

            service.MeetingAdd(meeting);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MeetingAdd_RangeInBusyTime_ExpectedException()
        {
            var meeting = GetMeetingAddTime(0,1);
            meeting.DateEnd = meeting.DateEnd.AddHours(1).AddMinutes(15);
            var repository = new SheduleRepository(new List<Meeting> { GetMeetingAddTime(0,2) });
            var service = new SheduleService(repository);

            service.MeetingAdd(meeting);
        }

        [TestMethod]
        public void MeetingAdd_CorrectMeeting_MeetingAdd()
        {
            var meeting = GetMeetingAddTime(0, 1);
            var repository = new SheduleRepository();
            var service = new SheduleService(repository);

            var result = service.MeetingAdd(meeting);

            Assert.AreEqual(meeting, result);
            Assert.AreEqual(1, repository.GetAll().Count);
            Assert.AreEqual(meeting, repository.Get(meeting.Id));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MeetingUpdate_UpdateNotExpectedMeeting_ExpectedException()
        {
            var meeting = GetMeetingNow();
            var repository = new SheduleRepository();
            var service = new SheduleService(repository);

            service.MeetingUpdate(meeting);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MeetingUpdate_DateBeginLowDateNow_ExpectedException()
        {
            var meeting = GetMeetingAddTime(1);
            var meeting2 = GetMeetingAddTime(-2);
            meeting2.Id = meeting.Id;
            var repository = new SheduleRepository(new List<Meeting> { meeting });
            var service = new SheduleService(repository);


            service.MeetingUpdate(meeting2);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MeetingUpdate_DateEndLowDateBegin_ExpectedException()
        {
            var meeting = GetMeetingAddTime(1);
            var meeting2 = GetMeetingAddTime(1);
            meeting2.Id = meeting.Id;
            meeting2.DateEnd = meeting.DateEnd.AddHours(-2);
            var repository = new SheduleRepository(new List<Meeting> { meeting });
            var service = new SheduleService(repository);


            service.MeetingUpdate(meeting2);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MeetingUpdate_RangeInBusyTime_ExpectedException()
        {
            var meeting = GetMeetingAddTime(0, 3);
            var meeting2 = GetMeetingAddTime(0, 3);
            meeting2.Id = meeting.Id;
            meeting2.DateBegin = meeting.DateBegin.AddHours(-2).AddMinutes(-10);
            var repository = new SheduleRepository(new List<Meeting> { GetMeetingAddTime(0, 1), meeting });
            var service = new SheduleService(repository);


            service.MeetingUpdate(meeting2);
        }

        [TestMethod]
        public void MeetingUpdate_CorrectUpdate_UpdatedItem()
        {
            var meeting = GetMeetingAddTime(0, 1);
            var meeting2 = GetMeetingAddTime(0, 1);
            meeting2.Id = meeting.Id;
            meeting2.DateEnd = meeting.DateEnd.AddHours(10).AddMinutes(-10);
            var repository = new SheduleRepository(new List<Meeting> { meeting });
            var service = new SheduleService(repository);

            var result = service.MeetingUpdate(meeting2);

            Assert.AreEqual(meeting2, result);
            Assert.AreEqual(meeting2, repository.Get(meeting.Id));
            Assert.AreNotEqual(meeting, result);
            Assert.AreNotEqual(meeting, repository.Get(meeting.Id));
        }

        [TestMethod]
        public void MeetingDelete_ExistItem_True()
        {
            var meeting = GetMeetingAddTime(0, 1);
            var repository = new SheduleRepository(new List<Meeting> { meeting });
            var service = new SheduleService(repository);

            var result = service.MeetingDelete(meeting);

            Assert.IsTrue(result);
            Assert.IsNull(repository.Get(meeting.Id));
            Assert.AreEqual(0, repository.GetAll().Count);
        }

        [TestMethod]
        public void MeetingDelete_NotExistItem_True()
        {
            var meeting = GetMeetingAddTime(0, 1);
            var repository = new SheduleRepository();
            var service = new SheduleService(repository);

            var result = service.MeetingDelete(meeting);

            Assert.IsTrue(result);
            Assert.IsNull(repository.Get(meeting.Id));
            Assert.AreEqual(0, repository.GetAll().Count);
        }

        [TestMethod]
        public void GetAll_2Item_2ItemsReturned()
        {
            var listMeeting = new List<Meeting> { GetMeetingAddTime(1), GetMeetingAddTime(2) };
            var repository = new SheduleRepository(listMeeting);
            var service = new SheduleService(repository);

            var result = service.GetAll();

            CollectionAssert.AreEqual(listMeeting, result.ToList());
        }

        [TestMethod]
        public void Get_2ItemsByDateAnd1OutOfDate_2ItemsReturned()
        {
            var testName = "test3";
            var listMeeting = new List<Meeting> { GetMeetingAddTime(1), GetMeetingAddTime(1, 1), GetMeetingAddTime(2, 0, testName) };
            var repository = new SheduleRepository(listMeeting);
            var service = new SheduleService(repository);

            var result = service.Get(DateTime.Now.AddDays(1));

            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            listMeeting.Remove(listMeeting.First(i => i.Name == testName));
            CollectionAssert.AreEqual(listMeeting, result.ToList());
        }

        [TestMethod]
        public void GetMeetingsForNotification_2ItemsByDateNotificationAnd1OutOfDate_2ItemsReturned()
        {
            var notification = DateTime.Now;
            var meeting = GetMeetingAddTime(2);
            meeting.Notification = notification;
            var meeting2 = GetMeetingAddTime(1);
            meeting2.Notification = notification;
            var meeting3 = GetMeetingAddTime(3);
            var listMeeting = new List<Meeting> { meeting, meeting2, meeting3 };
            var repository = new SheduleRepository(listMeeting);
            var service = new SheduleService(repository);

            var result = service.GetMeetingsForNotification(notification);

            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            listMeeting.Remove(meeting3);
            CollectionAssert.AreNotEqual(listMeeting, result.ToList());
            CollectionAssert.AreEqual(listMeeting.OrderBy(l => l.DateBegin).ToList(), result.ToList());
        }

        
    }
}
