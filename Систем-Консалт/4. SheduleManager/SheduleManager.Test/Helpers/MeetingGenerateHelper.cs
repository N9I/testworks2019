﻿using SheduleManager.Model;
using System;

namespace SheduleManager.Test
{
    public static class MeetingGenerateHelper
    {
        public static Meeting GetMeetingNow()
        {
            return new Meeting
            {
                DateBegin = DateTime.Now,
                DateEnd = DateTime.Now.AddHours(1),
                Name = "test"
            };
        }

        public static Meeting GetMeetingAddTime(int day, int hour = 0, string name = "test")
        {
            return new Meeting
            {
                DateBegin = DateTime.Now.AddDays(day).AddHours(hour),
                DateEnd = DateTime.Now.AddDays(day).AddHours(hour + 1),
                Name = name
            };
        }

        public static Meeting GetMeetingWitDiffereDayBeginAndEnd()
        {
            return new Meeting
            {
                DateBegin = DateTime.Now,
                DateEnd = DateTime.Now.AddDays(1),
                Name = "test"
            };
        }
    }
}
