﻿using System;
using SheduleManager.Enums;

namespace SheduleManager
{
    public interface IConsoleView
    {
        ExpectedEntry ExpectedEntry { get; }

        string Execute(ConsoleKey key);
        string Execute(string line);
        string Initialize();
        string GetNotifications(DateTime dateTime);
    }
}