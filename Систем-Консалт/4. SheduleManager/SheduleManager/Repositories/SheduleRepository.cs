﻿using SheduleManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SheduleManager.Repositories
{
    public class SheduleRepository : ISheduleRepository
    {
        private IList<Meeting> _entities = new List<Meeting>();

        public SheduleRepository()
        {}

        public SheduleRepository(List<Meeting> list)
        {
            _entities = list;
        }

        public Meeting Get(string id)
        {
            return _entities.SingleOrDefault(e => e.Id == id);
        }

        public IList<Meeting> GetAll()
        {
            return _entities.ToList();
        }

        public IList<Meeting> Get(DateTime date)
        {
            var searcheDate = date.Date;
            var list = _entities.Where(e => e.DateBegin.Date == searcheDate || e.DateEnd.Date == searcheDate);
            return list.OrderBy(e => e.DateBegin).ToList();
        }

        public Meeting Create(Meeting meeting)
        {
            _entities.Add(meeting);
            return meeting;
        }

        public Meeting Update(Meeting meeting)
        {
            var index = _entities.IndexOf(Get(meeting.Id));
            if (index!=-1)
                _entities[index] = meeting;
            return index != -1 ? meeting : null;
        }

        public void Delete (Meeting meeting)
        {
            var itemToRemove = Get(meeting.Id);
            if (itemToRemove != null)
                _entities.Remove(itemToRemove);
        }

        public bool RangeBusy(Meeting meeting)
        {
            return _entities.Any(e => e.Id != meeting.Id && e.DateBegin < meeting.DateBegin && e.DateEnd > meeting.DateBegin) ||
                _entities.Any(e => e.Id != meeting.Id && e.DateBegin < meeting.DateEnd && e.DateEnd > meeting.DateEnd) ||
                _entities.Any(e => e.Id != meeting.Id && e.DateBegin > meeting.DateBegin && e.DateBegin < meeting.DateEnd) ||
                _entities.Any(e => e.Id != meeting.Id && e.DateEnd > meeting.DateBegin && e.DateEnd < meeting.DateEnd);
        }

        public IList<Meeting> GetByNotificatoin(DateTime dateTime)
        {
            return _entities
                    .Where(e => e.Notification.AddMilliseconds(-e.Notification.Millisecond) == dateTime.AddMilliseconds(-dateTime.Millisecond))
                    .OrderBy(e => e.DateBegin).ToList();
        }
    }
}
