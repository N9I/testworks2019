﻿using SheduleManager.Model;
using System;
using System.Collections.Generic;

namespace SheduleManager.Repositories
{
    public interface ISheduleRepository
    {
        Meeting Get(string id);
        IList<Meeting> GetAll();
        IList<Meeting> Get(DateTime date);
        Meeting Create(Meeting meeting);
        Meeting Update(Meeting meeting);
        void Delete(Meeting meeting);
        bool RangeBusy(Meeting meeting);
        IList<Meeting> GetByNotificatoin(DateTime dateTime);
    }
}