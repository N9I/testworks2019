﻿namespace SheduleManager.Enums
{
    public enum ExpectedEntry
    {
        Key,
        Line,
        Exit
    }
}