﻿namespace SheduleManager
{
    internal enum Step
    {
        DateBegin,
        DateEnd,
        Name,
        NotificationQuestion,
        Notification,
        CreateException
    }
}