﻿namespace SheduleManager.Enums
{
    public enum SheduleState
    {
        MainMenu,
        FullShedule,
        SheduleByDay,
        CreateMeeting,
        UpdateMeeting,
        DeleteMeeting,
        ExportShedule
    }
}