﻿using SheduleManager.Model;
using SheduleManager.Repositories;
using System;
using System.Collections.Generic;

namespace SheduleManager.Services
{
    public class SheduleService : ISheduleService
    {
        private ISheduleRepository _repository;

        public SheduleService(ISheduleRepository repository) => _repository = repository;

        public Meeting MeetingAdd(Meeting item)
        {
            if (item.DateBegin < DateTime.Now)
                throw new Exception("Втреча не может быть назначена. Дата мероприятия не может быть меньше текущей");
            if (item.DateEnd < item.DateBegin)
                throw new Exception("Втреча не может быть назначена. Примерная дата окончания не может быть меньше даты начала");
            if (_repository.RangeBusy(item))
                throw new Exception("Втреча не может быть назначена. Указанное время занято");
            try
            {
                return _repository.Create(item);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Meeting MeetingUpdate(Meeting item)
        {
            var existedMeeting = _repository.Get(item.Id);
            if (existedMeeting == null)
                throw new Exception("Втреча не может быть изменена. Мероприятие не найдено в расписании");
            if (item.DateBegin < DateTime.Now)
                throw new Exception("Втреча не может быть назначена. Дата мероприятия не может быть меньше текущей");
            if (item.DateEnd < item.DateBegin)
                throw new Exception("Втреча не может быть назначена. Примерная дата окончания не может быть меньше даты начала");
            if (_repository.RangeBusy(item))
                throw new Exception("Втреча не может быть назначена. Указанное время занято");
            try
            {
                return _repository.Update(item);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool MeetingDelete(Meeting item)
        {
            try
            {
                _repository.Delete(item);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IList<Meeting> GetAll()
        {
            return _repository.GetAll();
        }

        public IList<Meeting> Get(DateTime date)
        {
            return _repository.Get(date);
        }

        public IList<Meeting> GetMeetingsForNotification(DateTime dateTime)
        {
            return _repository.GetByNotificatoin(dateTime);
        }
    }
}
