﻿using System;
using System.Collections.Generic;
using SheduleManager.Model;

namespace SheduleManager.Services
{
    public interface ISheduleService
    {
        IList<Meeting> Get(DateTime date);
        IList<Meeting> GetAll();
        IList<Meeting> GetMeetingsForNotification(DateTime dateTime);
        Meeting MeetingAdd(Meeting item);
        bool MeetingDelete(Meeting item);
        Meeting MeetingUpdate(Meeting item);
    }
}