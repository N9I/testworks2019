﻿using System;

namespace SheduleManager.Model
{
    public class Meeting
    {
        public string Id { get; set; } = Guid.NewGuid().GetHashCode().ToString("x");
        public DateTime DateBegin { get; set; }
        public DateTime DateEnd { get; set; }
        public DateTime Notification { get; set; }
        public string Name { get; set; }
    }
}
