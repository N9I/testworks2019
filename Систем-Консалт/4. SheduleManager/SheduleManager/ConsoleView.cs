﻿using SheduleManager.Enums;
using SheduleManager.Model;
using SheduleManager.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SheduleManager
{
    public class ConsoleView : IConsoleView
    {
        private const string _mainMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Просмотр расписания встреч, \r\n 2 - Просмотр расписания встреч на выбранный день, \r\n 3 - Создать новую встречу, \r\n 4 - Изменить встречу, \r\n 5 - Удалить встречу, \r\n 6 - Выгрузить расписание за выбранный день, \r\nESC - Выйти из программы";
        private const string _fullSheduleMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Вернуться в главное меню, \r\n 2 - Создать новую встречу, \r\n 3 - Изменить встречу, \r\n 4 - Удалить встречу, \r\nESC - Выйти из программы";
        private const string _SheduleByDateMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Вернуться в главное меню, \r\n 2 - Создать новую встречу, \r\n 3 - Изменить встречу, \r\n 4 - Удалить встречу, \r\n 5- Экспортировать в текстовый файл, \r\nESC - выйти из программы";
        private const string _SheduleByDateErrorMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Вернуться в главное меню, \r\n 2 - Повторить попытку, \r\nESC - Выйти из программы";
        private const string _SheduleCreateMeetingErrorMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Вернуться в главное меню, \r\n 2 - Повторить попытку, \r\nESC - Выйти из программы";
        private const string _UpdateMeetingMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Вернуться в главное меню, \r\n 2 - Изменить время начала, \r\n 3 - Изменить время окончания, \r\n 4 - Изменить время уведомления, \r\n 5 - Изменить описание (название), \r\nESC - Выйти из программы";
        private const string _UpdateMeetingErrorMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Вернуться в главное меню, \r\n 2 - Повторить попытку, \r\nESC - Выйти из программы";
        private const string _DeleteMeetingErrorMenu = "Для продолжения работы нажмите клавишу: \r\n 1 - Вернуться в главное меню, \r\n 2 - Повторить попытку, \r\nESC - Выйти из программы";
        private readonly ISheduleService _service;
        private SheduleState _currentState;
        private Step _currentStep;
        private IList<Meeting> _currentMeetingList;
        private DateTime? _selectedDate;
        private Meeting _currentMeeting;
        private bool _createError;

        public ExpectedEntry ExpectedEntry { get; private set; }

        public ConsoleView(ISheduleService service)
        {
            _service = service;
        }

        public string Initialize()
        {
            ExpectedEntry = ExpectedEntry.Key;
            _currentState = SheduleState.MainMenu;
            _currentMeetingList = null;
            _currentMeeting = null;
            _createError = false;
            _selectedDate = null;
            return _mainMenu;
        }

        public string Execute(ConsoleKey key)
        {
            var output = string.Empty;
            switch (_currentState)
            {
                case SheduleState.MainMenu:
                    switch (key)
                    {
                        case ConsoleKey.D1:
                            output = SheduleView();
                            break;
                        case ConsoleKey.D2:
                            output = SheduleByDayView();
                            break;
                        case ConsoleKey.D3:
                            output = CreateMeeting();
                            break;
                        case ConsoleKey.D4:
                            output = UpdateMeeting();
                            break;
                        case ConsoleKey.D5:
                            output = DeleteMeeting();
                            break;
                        case ConsoleKey.D6:
                            output = ExportShedule();
                            break;
                        case ConsoleKey.Escape:
                            ExpectedEntry = ExpectedEntry.Exit;
                            break;
                        default:
                            break;
                    }
                    break;
                case SheduleState.FullShedule:
                    switch (key)
                    {
                        case ConsoleKey.D1:
                            output = Initialize();
                            break;
                        case ConsoleKey.D2:
                            output = CreateMeeting();
                            break;
                        case ConsoleKey.D3:
                            output = UpdateMeeting();
                            break;
                        case ConsoleKey.D4:
                            output = DeleteMeeting();
                            break;
                        case ConsoleKey.Escape:
                            ExpectedEntry = ExpectedEntry.Exit;
                            break;
                        default:
                            break;
                    }
                    break;
                case SheduleState.SheduleByDay:
                    switch (key)
                    {
                        case ConsoleKey.D1:
                            output = Initialize();
                            break;
                        case ConsoleKey.D2:
                            output = _currentMeetingList == null ? SheduleByDayView() : CreateMeeting();
                            break;
                        case ConsoleKey.D3:
                            output = UpdateMeeting();
                            break;
                        case ConsoleKey.D4:
                            output = DeleteMeeting();
                            break;
                        case ConsoleKey.D5:
                            output = ExportShedule();
                            break;
                        case ConsoleKey.Escape:
                            ExpectedEntry = ExpectedEntry.Exit;
                            break;
                        default:
                            break;
                    }
                    break;
                case SheduleState.CreateMeeting:
                    switch (key)
                    {
                        case ConsoleKey.D1:
                            output = Initialize();
                            break;
                        case ConsoleKey.D2:
                            output = CreateMeeting();
                            break;
                        case ConsoleKey.Y:
                            if (_currentStep == Step.NotificationQuestion)
                                output = CreateMeeting("y");
                            else if (_currentStep == Step.CreateException)
                                output = UpdateMeeting();
                            break;
                        case ConsoleKey.N:
                            if (_currentStep == Step.NotificationQuestion)
                                output = CreateMeeting("n");
                            else if (_currentStep == Step.CreateException)
                                output = Initialize();
                            break;
                        case ConsoleKey.Escape:
                            ExpectedEntry = ExpectedEntry.Exit;
                            break;
                        default:
                            break;
                    }
                    break;
                case SheduleState.UpdateMeeting:
                    switch (key)
                    {
                        case ConsoleKey.D1:
                            output = Initialize();
                            break;
                        case ConsoleKey.D2:
                            if (_currentMeeting != null)
                                _currentStep = Step.DateBegin;
                            output = UpdateMeeting();
                            break;
                        case ConsoleKey.D3:
                            if (_currentMeeting != null)
                                _currentStep = Step.DateEnd;
                            output = UpdateMeeting();
                            break;
                        case ConsoleKey.D4:
                            if (_currentMeeting != null)
                                _currentStep = Step.Notification;
                            output = UpdateMeeting();
                            break;
                        case ConsoleKey.D5:
                            if (_currentMeeting != null)
                                _currentStep = Step.Name;
                            output = UpdateMeeting();
                            break;
                        case ConsoleKey.Y:
                            if (_currentStep == Step.CreateException)
                                output = UpdateMeeting();
                            break;
                        case ConsoleKey.N:
                            if (_currentStep == Step.CreateException)
                                output = Initialize();
                            break;
                        case ConsoleKey.Escape:
                            ExpectedEntry = ExpectedEntry.Exit;
                            break;
                        default:
                            break;
                    }
                    break;
                case SheduleState.DeleteMeeting:
                    switch (key)
                    {
                        case ConsoleKey.D1:
                            output = Initialize();
                            break;
                        case ConsoleKey.D2:
                            output = DeleteMeeting();
                            break;
                        case ConsoleKey.Escape:
                            ExpectedEntry = ExpectedEntry.Exit;
                            break;
                        default:
                            break;
                    }
                    break;
            }
            return output;
        }

        public string Execute(string line)
        {
            var output = string.Empty;
            switch (_currentState)
            {
                case SheduleState.SheduleByDay:
                    output = SheduleByDayView(line);
                    break;
                case SheduleState.CreateMeeting:
                    output = CreateMeeting(line);
                    break;
                case SheduleState.UpdateMeeting:
                    output = UpdateMeeting(line);
                    break;
                case SheduleState.DeleteMeeting:
                    output = DeleteMeeting(line);
                    break;
            }
            return output;
        }

        private string SheduleView()
        {
            _currentState = SheduleState.FullShedule;
            ExpectedEntry = ExpectedEntry.Key;
            _currentMeetingList = _service.GetAll();
            return $"Список встреч\r\n\r\n{PrintMeetings(_currentMeetingList)}\r\n\r\n{_fullSheduleMenu}";
        }

        private string SheduleByDayView(string input = "")
        {
            _currentState = SheduleState.SheduleByDay;
            var output = "Список встреч";
            if (input == string.Empty)
            {
                output = output + "\r\n\r\nВведите дату:";
                ExpectedEntry = ExpectedEntry.Line;
            }
            else
            {
                if (DateTime.TryParse(input, out DateTime userDateTime))
                {
                    _currentMeetingList = _service.Get(userDateTime);
                    _selectedDate = userDateTime;
                    output = output + $": {userDateTime.Date.ToString("dd.MM.yyyy")}\r\n\r\n{ PrintMeetings(_currentMeetingList)}\r\n\r\n{ _SheduleByDateMenu }";
                    ExpectedEntry = ExpectedEntry.Key;
                }
                else
                {
                    _currentMeetingList = null;
                    output = output + $"\r\n\r\nОшибка при считывании даты. \r\n\r\n{ _SheduleByDateErrorMenu }";
                    ExpectedEntry = ExpectedEntry.Key;
                }
            }
            return output;
        }

        private string CreateMeeting(string input = "")
        {
            if (_currentMeeting == null)
            {
                _currentMeeting = new Meeting();
                _currentStep = Step.DateBegin;
            }
            _currentState = SheduleState.CreateMeeting;
            var output = "Создание встречи";
            if (input == string.Empty)
            {
                ExpectedEntry = ExpectedEntry.Line;
                switch (_currentStep)
                {
                    case Step.DateBegin:
                        output = output + "\r\n\r\nВведите дату и время начала встречи:";
                        break;
                    case Step.DateEnd:
                        output = output + "\r\n\r\nВведите дату и время примерного окончания встречи:";
                        break;
                    case Step.Name:
                        output = output + "\r\n\r\nВведите название встречи или имя с кем назначена встреча:";
                        break;
                    case Step.NotificationQuestion:
                        output = output + "\r\n\r\nЖелаете установить напоминание? (y/n)";
                        ExpectedEntry = ExpectedEntry.Key;
                        break;
                    case Step.Notification:
                        output = output + "\r\n\r\nВведите дату и время напоминания";
                        break;
                }
            }
            else
            {
                DateTime userDateTime;
                switch (_currentStep)
                {
                    case Step.DateBegin:
                        if (DateTime.TryParse(input, out userDateTime))
                        {
                            _currentMeeting.DateBegin = userDateTime;
                            _currentStep = Step.DateEnd;
                            output = CreateMeeting();
                        }
                        else
                        {
                            output = output + $"\r\n\r\nОшибка при считывании даты. \r\n\r\n{ _SheduleCreateMeetingErrorMenu }";
                            ExpectedEntry = ExpectedEntry.Key;
                        }
                        break;
                    case Step.DateEnd:
                        if (DateTime.TryParse(input, out userDateTime))
                        {
                            _currentMeeting.DateEnd = userDateTime;
                            _currentStep = Step.Name;
                            output = CreateMeeting();
                        }
                        else
                        {
                            output = output + $"\r\n\r\nОшибка при считывании даты. \r\n\r\n{ _SheduleCreateMeetingErrorMenu }";
                            ExpectedEntry = ExpectedEntry.Key;
                        }
                        break;
                    case Step.Name:
                        _currentMeeting.Name = string.IsNullOrEmpty(input) ? "-" : input;
                        _currentStep = Step.NotificationQuestion;
                        output = CreateMeeting();
                        break;
                    case Step.NotificationQuestion:
                        if (input == "y")
                        {
                            _currentStep = Step.Notification;
                            output = CreateMeeting();
                        }
                        else
                        {
                            output = MeetingAdd();
                        }
                        break;
                    case Step.Notification:
                        if (DateTime.TryParse(input, out userDateTime))
                        {
                            output = MeetingAdd(userDateTime);
                        }
                        else
                        {
                            output = output + $"\r\n\r\nОшибка при считывании даты. \r\n\r\n{ _SheduleCreateMeetingErrorMenu }";
                            ExpectedEntry = ExpectedEntry.Key;
                        }
                        break;
                }
            }
            return output;
        }

        private string MeetingAdd(DateTime? NotificationTime = null)
        {
            var output = string.Empty;
            if (NotificationTime != null)
                _currentMeeting.Notification = (DateTime)NotificationTime;
            try
            {
                _service.MeetingAdd(_currentMeeting);
                output = $"Встреча записана\r\n\r\n{ Initialize() }";
            }
            catch (Exception e)
            {
                _createError = true;
                if (e.Message.Contains("Втреча"))
                {
                    ExpectedEntry = ExpectedEntry.Key;
                    _currentStep = Step.CreateException;
                    output = $"\r\n\r\n{e.Message}\r\n\r\nВнести изменения в параметры встречи и попробовать сохранить снова? (y/n)";
                }
                else
                    output = $"Не опознанная ошибка, встреча не добавлена\r\n\r\n{ Initialize() }";
            }
            return output;
        }

        private string UpdateMeeting(string input = "")
        {
            _currentState = SheduleState.UpdateMeeting;
            var output = "Внесение изменений в расписание";
            if (string.IsNullOrEmpty(input))
            {
                if (_currentMeeting == null)
                {
                    if (_currentMeetingList == null)
                    {
                        ExpectedEntry = ExpectedEntry.Key;
                        output = output + $"\r\n\r\nВыберите встречу для внесения изменений:\r\n{SheduleView()}";
                    }
                    else
                    {
                        ExpectedEntry = ExpectedEntry.Line;
                        output = output + $"\r\n\r\n{PrintMeetings(_currentMeetingList)}\r\n\r\nУкажите номер выбранной встречи:";
                    }
                }
                else
                {
                    ExpectedEntry = ExpectedEntry.Line;
                    switch (_currentStep)
                    {
                        case Step.CreateException:
                        case Step.NotificationQuestion:
                            ExpectedEntry = ExpectedEntry.Key;
                            output = output + $"\r\n\r\n{PrintMeeting(_currentMeeting)}\r\n\r\n{ _UpdateMeetingMenu }";
                            break;
                        case Step.DateBegin:
                            output = output + "\r\n\r\nВведите дату и время начала встречи:";
                            break;
                        case Step.DateEnd:
                            output = output + "\r\n\r\nВведите дату и время примерного окончания встречи:";
                            break;
                        case Step.Name:
                            output = output + "\r\n\r\nВведите название встречи или имя с кем назначена встреча:";
                            break;
                        case Step.Notification:
                            output = output + "\r\n\r\nВведите дату и время напоминания";
                            break;
                    }
                }
            }
            else
            {
                if (_currentMeeting == null)
                {
                    ExpectedEntry = ExpectedEntry.Key;
                    if (int.TryParse(input, out int index))
                    {
                        if (_currentMeetingList.Count >= index)
                        {
                            _currentMeeting = _currentMeetingList[index - 1];
                            output = UpdateMeeting();
                        }
                        else
                            output = output + $"\r\n\r\nОшибка при вводе номера встречи в списке \r\n\r\n{ _UpdateMeetingErrorMenu }";

                    }
                    else
                    {
                        output = output + $"\r\n\r\nОшибка при вводе номера встречи в списке \r\n\r\n{ _UpdateMeetingErrorMenu }";
                    }
                }
                else
                {
                    ExpectedEntry = ExpectedEntry.Key;
                    DateTime userDateTime;
                    switch (_currentStep)
                    {
                        case Step.DateBegin:
                            if (DateTime.TryParse(input, out userDateTime))
                            {
                                _currentMeeting.DateBegin = userDateTime;
                                output = output + MeetingUpdate();
                            }
                            else
                            {
                                output = output + $"\r\n\r\nОшибка при считывании даты. \r\n\r\n{ _UpdateMeetingErrorMenu }";
                            }
                            break;
                        case Step.DateEnd:
                            if (DateTime.TryParse(input, out userDateTime))
                            {
                                _currentMeeting.DateEnd = userDateTime;
                                output = output + MeetingUpdate();
                            }
                            else
                            {
                                output = output + $"\r\n\r\nОшибка при считывании даты. \r\n\r\n{ _UpdateMeetingErrorMenu }";
                            }
                            break;
                        case Step.Name:
                            _currentMeeting.Name = string.IsNullOrEmpty(input) ? "-" : input;
                            output = output + MeetingUpdate();
                            break;
                        case Step.Notification:
                            if (DateTime.TryParse(input, out userDateTime))
                            {
                                _currentMeeting.Notification = userDateTime;
                                output = output + MeetingUpdate();
                            }
                            else
                            {
                                output = output + $"\r\n\r\nОшибка при считывании даты. \r\n\r\n{ _UpdateMeetingErrorMenu }";
                            }
                            break;
                    }
                }
            }
            return output;
        }

        private string MeetingUpdate()
        {
            var output = string.Empty;
            try
            {
                if (_createError)
                {
                    _service.MeetingAdd(_currentMeeting);
                    _createError = false;
                }
                else
                    _service.MeetingUpdate(_currentMeeting);
                output = $"Изменения сохранены\r\n\r\n{PrintMeeting(_currentMeeting)}\r\n\r\n{ _UpdateMeetingMenu }";
                _currentStep = Step.NotificationQuestion;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Втреча"))
                {
                    _currentStep = Step.CreateException;
                    output = $"\r\n\r\n{e.Message}\r\n\r\nВнести изменения в параметры встречи и попробовать сохранить снова? (y/n)";
                }
                else
                    output = $"Не опознанная ошибка, встреча не изменена\r\n\r\n{ Initialize() }";
            }
            return output;
        }

        private string DeleteMeeting(string input = "")
        {
            _currentState = SheduleState.DeleteMeeting;
            var output = "Удаление встречи из расписания";
            if (string.IsNullOrEmpty(input))
            {
                if (_currentMeeting == null)
                {
                    if (_currentMeetingList == null)
                    {
                        ExpectedEntry = ExpectedEntry.Line;
                        output = output + $"\r\n\r\nВыберите встречу для удаления:\r\n{SheduleView()}";
                    }
                    else
                    {
                        ExpectedEntry = ExpectedEntry.Line;
                        output = output + $"\r\n\r\n{PrintMeetings(_currentMeetingList)}\r\n\r\nУкажите номер выбранной встречи:";
                    }
                }
                else
                {
                    output = MeetingDelete();
                }
            }
            else
            {
                if (_currentMeeting == null)
                {
                    ExpectedEntry = ExpectedEntry.Key;
                    if (int.TryParse(input, out int index))
                    {
                        if (_currentMeetingList.Count >= index)
                        {
                            _currentMeeting = _currentMeetingList[index - 1];
                            output = MeetingDelete();
                        }
                        else
                            output = output + $"\r\n\r\nОшибка при вводе номера встречи в списке \r\n\r\n{ _DeleteMeetingErrorMenu }";

                    }
                    else
                    {
                        output = output + $"\r\n\r\nОшибка при вводе номера встречи в списке \r\n\r\n{ _DeleteMeetingErrorMenu }";
                    }
                }
                else
                {
                    output = Initialize();
                }
            }
            return output;
        }

        private string MeetingDelete()
        {
            var output = string.Empty;
            try
            {
                _service.MeetingDelete(_currentMeeting);
                output = $"Встреча удалена\r\n\r\n{ Initialize() }";
            }
            catch (Exception)
            {
                output = $"Не опознанная ошибка\r\n\r\n{ Initialize() }";
            }
            return output;
        }

        private string ExportShedule()
        {
            _currentState = SheduleState.ExportShedule;
            var output = "Выгрузка расписания встреч";
            if (_currentMeetingList == null || _selectedDate == null)
            {
                ExpectedEntry = ExpectedEntry.Key;
                output = output + $"\r\n\r\nВыберите встречи для выгрузки:\r\n{SheduleByDayView()}";
            }
            else
            {
                try
                {
                    var text = $"Список встреч: {((DateTime)_selectedDate).Date.ToString("dd.MM.yyyy")}\r\n\r\n{ PrintMeetings(_currentMeetingList)}\r\n\r\n{ _SheduleByDateMenu }";
                    var filename = $@"Shedule {((DateTime)_selectedDate).Date.ToString("dd.MM.yyyy")}.txt";
                    using (var fstream = new FileStream(filename, FileMode.Create))
                    {
                        byte[] input = Encoding.Default.GetBytes(text);
                        fstream.Write(input, 0, input.Length);
                        output = output + $"\r\n\r\nТекст записан в файл {filename}\r\n\r\n{Initialize()}";
                    }
                }
                catch (Exception)
                {
                    output = $"Не опознанная ошибка\r\n\r\n{ Initialize() }";
                }
                return output;
            }
            return output;
        }

        private static string PrintMeeting(Meeting meeting)
        {
            var table = "Дата начала - Дата окончания - Уведомление - Цель встречи";
            table = table + $"\r\n{meeting.DateBegin.ToString("dd.MM.yyyy HH.mm")} |  {meeting.DateEnd.ToString("dd.MM.yyyy HH.mm")} |  {meeting.Notification.ToString("dd.MM.yyyy HH.mm")} |  {meeting.Name}";
            return table;
        }

        private static string PrintMeetings(IList<Meeting> meetings)
        {
            var table = "№п/п - Дата начала - Дата окончания - Уведомление - Цель встречи";
            var counter = 0;
            foreach (var meeting in meetings)
            {
                counter++;
                table = table + $"\r\n{counter}. | {meeting.DateBegin.ToString("dd.MM.yyyy HH.mm")} |  {meeting.DateEnd.ToString("dd.MM.yyyy HH.mm")} |  {meeting.Notification.ToString("dd.MM.yyyy HH.mm")} |  {meeting.Name}";
            }
            return table;
        }

        public string GetNotifications(DateTime dateTime)
        {
            var result = string.Empty;
            var meetingList = _service.GetMeetingsForNotification(dateTime);
            if (meetingList != null && meetingList.Any())
            {
                result = $"\r\n\r\nУведомление о встрече(-ах)\r\n\r\n{PrintMeetings(meetingList)}";
            }
            return result;
        }
    }
}
