﻿using Microsoft.Extensions.DependencyInjection;
using SheduleManager.Enums;
using System;
using System.Threading;

namespace SheduleManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Добро пожаловать в SheduleManager.");
            try
            {
                Settings.SettingProgram(out ServiceProvider container);
                var view = container.GetService<IConsoleView>();
                new Thread(() => Alarm(view)).Start();
                var output = view.Initialize();
                while (view.ExpectedEntry != ExpectedEntry.Exit)
                {
                    Console.WriteLine(output);
                    switch (view.ExpectedEntry)
                    {
                        case ExpectedEntry.Key:
                            output = view.Execute(Console.ReadKey(true).Key);
                            break;
                        case ExpectedEntry.Line:
                            output = view.Execute(Console.ReadLine());
                            break;

                    }
                    Console.Clear();
                }
                Environment.Exit(1);
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("Что-то пошло не так, обратитесь к разработчикам. \r\nПриносим извинения за предоставленные неудобства.");
            }
        }

        private static void Alarm(IConsoleView view)
        {
            try
            {
                while (true)
                {
                    var dateTime = DateTime.Now;
                    var notifications = view.GetNotifications(dateTime.AddMilliseconds(-dateTime.Millisecond));
                    if (!string.IsNullOrEmpty(notifications))
                        Console.WriteLine(notifications);
                    Thread.Sleep(1000);
                }
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("Что-то пошло не так, обратитесь к разработчикам. \r\nПриносим извинения за предоставленные неудобства.");
            }
        }
    }
}
