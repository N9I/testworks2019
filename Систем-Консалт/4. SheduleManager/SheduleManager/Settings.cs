﻿using Microsoft.Extensions.DependencyInjection;
using SheduleManager.Repositories;
using SheduleManager.Services;

namespace SheduleManager
{
    public static class Settings
    {
        public static void SettingProgram(out ServiceProvider container)
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection
                .AddSingleton<ISheduleRepository, SheduleRepository>()
                .AddScoped<ISheduleService, SheduleService>()
                .AddScoped<IConsoleView, ConsoleView>();

            container = serviceCollection.BuildServiceProvider();
        }
    }
}
