﻿CREATE Table Sellers (ID int,  Surname nvarchar(100), Name nvarchar(100));

CREATE Table Sales (ID int, IDSel int, IDProd int, Date DateTime, Quantity float);

CREATE Table Products (ID int, Name nvarchar(100), Price float);

CREATE Table Arrivals (ID int, IDProd int, Date DateTime);

/*
Написать SQL-запрос, который возвращает объем продаж в количественном выражении в разрезе сотрудников за период с 01.10.2013 по 07.10.2013:
●	Фамилия и имя сотрудника;
●	Объем продаж сотрудника.
Данные отсортировать по фамилии и имени сотрудника.
*/



SELECT s.Surname, s.Name, SUM(sl.Quantity * p.Price) as Volume
  
  FROM  Sellers s

        LEFT JOIN Sales sl ON sl.IDSel = s.ID

        LEFT JOIN Products p ON sl.IDProd = p.ID

  WHERE sl.Date BETWEEN '20131001' AND '20131007'

  GROUP BY s.Surname, s.Name, s.ID

  ORDER BY s.Surname, s.Name;



/*
Написать SQL-запрос, который возвращает процент объема продаж в разрезе сотрудников и продукции за период с 01.10.2013 по 07.10.2013:
●	Наименование продукции;
●	Фамилия и имя сотрудника;
●	Процент продаж сотрудником данного вида продукции (продажи сотрудника данной продукции/общее число продаж данной продукции).
В выборку должна попадать продукция, поступившая за период с 07.09.2013 по 07.10.2013.
Данные отсортировать по наименованию продукции, фамилии и имени сотрудника.
*/

SELECT p.Name, s.Surname, s.Name, Count(*)/(Count(*) OVER (PARTITION BY p.ID)) as Part

  FROM  Sellers s

        LEFT JOIN Sales sl ON sl.IDSel = s.ID

        LEFT JOIN Products p ON sl.IDProd = p.ID

        LEFT JOIN Arrivals a ON a.IDProd = p.ID

  WHERE sl.Date BETWEEN '20131001' AND '20131007'

        AND a.Date BETWEEN '20130907' AND '20131007'

  GROUP BY s.Surname, s.Name, s.ID, p.Name, p.ID

  ORDER BY p.Name, s.Surname, s.Name;
