$(function() {
	$('#fotorama').fotorama({
		width: 700,
		height: 490
	});
});

'use strict';

const e = React.createElement;

class Page extends React.Component {
	constructor(props) {
		super(props);
		this.state = props;
	}
	
	render() {
		var category = e('div', {class: 'row category'}, this.state.Category);
		var galery = e('div', {class: 'galery col-md-6 fotorama'}, this.state.Galery.map((img) => e('img', {src: img})));
		var objectName = e('div', {class: 'object row'}, e('span', null, this.state.Name));
		var deadline = e('div', {class: 'deadline row'}, e('span', null, "срок сдачи: "+this.state.Deadline));
		var advantages = e('div', {class: 'advantages'}, this.state.Advantages.map((s) => e('div', {class: 'btn btn-outline-dark advantage'}, s)));
		var initial = e('div', {class: 'initial row'}, ([e('div', {class: 'initialspan'}, "Первоначальный взнос"), "от "+ Math.floor(this.state.Initial * this.state.Price / 100).toLocaleString() +" ₽/мес"]).map((s) => e('div', {class: 'initialfee col-md-6'}, s)));
		var price = e('div', {class: 'price row'}, [e('div', {class: 'pricen'}, "Цена"), e('span', {class: 'pricem'}, "от "+this.state.Price.toLocaleString()+" руб.")]);
		var checkSumm = e('button', {class: 'btn btn-danger checksumm'}, "Узнать цену")
		var description =  e('div', {class: 'description col-md-6'}, [objectName, deadline, advantages, initial, price, checkSumm]);
		var flatBox = e('div', {class: 'row flatBox test'}, [galery, description]);
		return e('div', {class: 'row outer'}, e('div', {class: 'page col-md-12'}, [category,flatBox]));
	}
}

var message = JSON.stringify({"operation":"getFlatInfo"});
$("#loader").show();
$.post('FlatController.php', {message:message}, 
	function(message) {
		var data = JSON.parse(message);
		ReactDOM.render(e(Page, data), document.getElementById('offer'));
		$("#loader").hide();
	}
);